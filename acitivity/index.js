fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json() )
.then((json) => {
	let mapped = Array()
	Object.values(json).map( function(i) {
		mapped.push(i.title)			
	})	
	console.log( Array(mapped) )
});




//================GET=====================
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => (
	console.log(json),
//	console.log("The item " + json.title + " on the list has a status of " + json.completed);
	console.log(`The item "${json.title}" on the list has a status of "${json.completed}"` )
)
);



//=========POST=============
fetch('https://jsonplaceholder.typicode.com/todos', {
method: 'POST',
headers: {'Content-type': 'application/json',
},
		body: JSON.stringify ({
		completed: false ,
		id: 201,
		title: "Created To DO List item",
		userId: 1
	})
}).then((response) => response.json())
.then((json) => console.log(json));


//=========PUT============
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {'Content-type': 'application/json',},
	body: JSON.stringify({
	  	title: "Updated To DO List item",
	  	status: "Pending",
	  	id: 1,
	  	description: "To update my to-do-list with different data structure",
	  	dateCompleted: "Pending",
	  	userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//==========PATCH==============
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({

	  	status: "Completed",
	  	dateCompleted: "01/11/2023"
	  	
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



//============DELETE=============

fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE'
});
